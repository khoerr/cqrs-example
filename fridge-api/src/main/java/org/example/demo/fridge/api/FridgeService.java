package org.example.demo.fridge.api;

import akka.Done;
import akka.NotUsed;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.broker.kafka.KafkaProperties;

import static com.lightbend.lagom.javadsl.api.Service.*;

/**
 * The Fridge service interface.
 * <p>
 * This describes everything that Lagom needs to know about how to serve and
 * consume the Fridge.
 */
public interface FridgeService extends Service {

  /**
   * Example: curl http://localhost:9000/api/contents/milk
   */
  ServiceCall<NotUsed, String> contents(String item);


  /**
   * Example: curl -H "Content-Type: application/json" -X POST -d '{"amount":
   * "Half gallon"}' http://localhost:9000/api/store/milk
   */
  ServiceCall<ItemAmount, Done> store(String item);


  /**
   * This gets published to Kafka.
   */
  Topic<FridgeEvent> fridgeEvents();

  @Override
  default Descriptor descriptor() {
    // @formatter:off
    return named("fridge").withCalls(
        pathCall("/api/contents/:id",  this::contents),
        pathCall("/api/store/:id", this::store)
      ).withTopics(
          topic("fridge-events", this::fridgeEvents)
          // Kafka partitions messages, messages within the same partition will
          // be delivered in order, to ensure that all messages for the same user
          // go to the same partition (and hence are delivered in order with respect
          // to that user), we configure a partition key strategy that extracts the
          // name as the partition key.
          .withProperty(KafkaProperties.partitionKeyStrategy(), FridgeEvent::getName)
        ).withAutoAcl(true);
    // @formatter:on
  }
}
