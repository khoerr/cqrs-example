package org.example.demo.fridge.api;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.google.common.base.Preconditions;
import lombok.Value;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type")
@JsonSubTypes({
  @JsonSubTypes.Type(value = FridgeEvent.ItemAmountChanged.class, name = "item-amount-changed")
})
public interface FridgeEvent {

  String getName();

  @Value
  final class ItemAmountChanged implements FridgeEvent {
    public final String name;
    public final String amount;

    @JsonCreator
    public ItemAmountChanged(String name, String amount) {
        this.name = Preconditions.checkNotNull(name, "name");
        this.amount = Preconditions.checkNotNull(amount, "amount");
    }
  }
}
