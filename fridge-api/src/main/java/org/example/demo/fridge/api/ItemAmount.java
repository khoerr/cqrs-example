package org.example.demo.fridge.api;

import lombok.Value;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;

@Value
@JsonDeserialize
public class ItemAmount {

   public final String amount;

   @JsonCreator
   public ItemAmount(String amount) {
      this.amount = Preconditions.checkNotNull(amount, "amount");
   }
}
