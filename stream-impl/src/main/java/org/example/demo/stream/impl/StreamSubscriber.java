package org.example.demo.stream.impl;

import akka.Done;
import akka.stream.javadsl.Flow;
import org.example.demo.fridge.api.FridgeEvent;
import org.example.demo.fridge.api.FridgeService;

import javax.inject.Inject;
import java.util.concurrent.CompletableFuture;

/**
 * This subscribes to the FridgeService event stream.
 */
public class StreamSubscriber {

  @Inject
  public StreamSubscriber(FridgeService fridgeService, StreamRepository repository) {
    // Create a subscriber
    fridgeService.fridgeEvents().subscribe()
      // And subscribe to it with at least once processing semantics.
      .atLeastOnce(
        // Create a flow that emits a Done for each message it processes
        Flow.<FridgeEvent>create().mapAsync(1, event -> {

          if (event instanceof FridgeEvent.ItemAmountChanged) {
            FridgeEvent.ItemAmountChanged messageChanged = (FridgeEvent.ItemAmountChanged) event;
            // Update the message
            return repository.updateMessage(messageChanged.getName(), messageChanged.getAmount());

          } else {
            // Ignore all other events
            return CompletableFuture.completedFuture(Done.getInstance());
          }
        })
      );

  }
}
