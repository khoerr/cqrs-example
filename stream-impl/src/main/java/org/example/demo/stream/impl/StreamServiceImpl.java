package org.example.demo.stream.impl;

import akka.NotUsed;
import akka.stream.javadsl.Source;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import org.example.demo.fridge.api.FridgeService;
import org.example.demo.stream.api.StreamService;

import javax.inject.Inject;

import static java.util.concurrent.CompletableFuture.completedFuture;

/**
 * Implementation of the FridgeStream.
 */
public class StreamServiceImpl implements StreamService {

  private final FridgeService fridgeService;
  private final StreamRepository repository;

  @Inject
  public StreamServiceImpl(FridgeService fridgeService, StreamRepository repository) {
    this.fridgeService = fridgeService;
    this.repository = repository;
  }

  @Override
  public ServiceCall<Source<String, NotUsed>, Source<String, NotUsed>> directStream() {
    return hellos -> completedFuture(
      hellos.mapAsync(8, name ->  fridgeService.contents(name).invoke()));
  }

  @Override
  public ServiceCall<Source<String, NotUsed>, Source<String, NotUsed>> autonomousStream() {
    return hellos -> completedFuture(
        hellos.mapAsync(8, name -> repository.getAmount(name).thenApply(amount -> "Item " + name + " has an amount of: " + amount.orElse("Item not found")))
    );
  }
}
