package org.example.demo.stream.impl;

import akka.Done;
import com.lightbend.lagom.javadsl.persistence.cassandra.CassandraSession;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

@Singleton
public class StreamRepository {
  private final CassandraSession uninitialisedSession;

  // Will return the session when the Cassandra tables have been successfully created
  private volatile CompletableFuture<CassandraSession> initialisedSession;

  @Inject
  public StreamRepository(CassandraSession uninitialisedSession) {
    this.uninitialisedSession = uninitialisedSession;
    // Eagerly create the session
    session();
  }

  private CompletionStage<CassandraSession> session() {
    // If there's no initialised session, or if the initialised session future completed
    // with an exception, then reinitialise the session and attempt to create the tables
    if (initialisedSession == null || initialisedSession.isCompletedExceptionally()) {
      initialisedSession = uninitialisedSession.executeCreateTable(
          "CREATE TABLE IF NOT EXISTS item_amount (name text PRIMARY KEY, amount text)"
      ).thenApply(done -> uninitialisedSession).toCompletableFuture();
    }
    return initialisedSession;
  }

  public CompletionStage<Done> updateMessage(String name, String message) {
    return session().thenCompose(session ->
        session.executeWrite("INSERT INTO item_amount (name, amount) VALUES (?, ?)",
            name, message)
    );
  }

  public CompletionStage<Optional<String>> getAmount(String name) {
    return session().thenCompose(session ->
        session.selectOne("SELECT amount FROM item_amount WHERE name = ?", name)
    ).thenApply(maybeRow -> maybeRow.map(row -> row.getString("amount")));
  }
}
