package org.example.demo.stream.api;

import static com.lightbend.lagom.javadsl.api.Service.named;
import static com.lightbend.lagom.javadsl.api.Service.namedCall;

import akka.NotUsed;
import akka.stream.javadsl.Source;
import com.lightbend.lagom.javadsl.api.Descriptor;
import com.lightbend.lagom.javadsl.api.Service;
import com.lightbend.lagom.javadsl.api.ServiceCall;

/**
 * The stream interface.
 * <p>
 * This describes everything that Lagom needs to know about how to serve and
 * consume the FridgeStream service.
 */
public interface StreamService extends Service {

  /**
   * This stream is implemented by asking the fridge service directly to say
   * fridge to each passed in name. It requires the fridge service to be up
   * and running to function.
   */
  ServiceCall<Source<String, NotUsed>, Source<String, NotUsed>> directStream();

  /**
   * This stream is implemented autonomously, it uses its own store, populated
   * by subscribing to the events published by the fridge service, to say fridge
   * to each passed in name. It can function even when the fridge service is
   * down.
   */
  ServiceCall<Source<String, NotUsed>, Source<String, NotUsed>> autonomousStream();

  @Override
  default Descriptor descriptor() {
    return named("stream")
            .withCalls(
              namedCall("direct-stream", this::directStream),
              namedCall("auto-stream", this::autonomousStream)
            ).withAutoAcl(true);
  }
}
