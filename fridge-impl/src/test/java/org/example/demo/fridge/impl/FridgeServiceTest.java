package org.example.demo.fridge.impl;

import static com.lightbend.lagom.javadsl.testkit.ServiceTest.defaultSetup;
import static com.lightbend.lagom.javadsl.testkit.ServiceTest.withServer;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.junit.Assert.assertEquals;

import org.example.demo.fridge.api.FridgeService;
import org.example.demo.fridge.api.ItemAmount;
import org.junit.Test;


public class FridgeServiceTest {

  @Test
  public void shouldStorePersonalizedGreeting() throws Exception {
    withServer(defaultSetup().withCassandra(), server -> {
      FridgeService service = server.client(FridgeService.class);

      String msg1 = service.contents("soy sauce").invoke().toCompletableFuture().get(5, SECONDS);
      assertEquals("Item soy sauce has an amount of: Item not found", msg1); // default greeting

      service.store("soy sauce").invoke(new ItemAmount("20ml")).toCompletableFuture().get(5, SECONDS);
      String msg2 = service.contents("soy sauce").invoke().toCompletableFuture().get(5, SECONDS);
      assertEquals("Item soy sauce has an amount of: 20ml", msg2);

      String msg3 = service.contents("pizza").invoke().toCompletableFuture().get(5, SECONDS);
      assertEquals("Item pizza has an amount of: Item not found", msg3); // default greeting
    });
  }

}
