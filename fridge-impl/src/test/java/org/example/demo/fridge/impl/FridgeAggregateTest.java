package org.example.demo.fridge.impl;

import akka.actor.testkit.typed.javadsl.TestKitJunitResource;
import akka.actor.testkit.typed.javadsl.TestProbe;
import akka.actor.typed.ActorRef;
import akka.cluster.sharding.typed.javadsl.EntityContext;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.UUID;

public class FridgeAggregateTest {
  private static final String inmemConfig =
      "akka.persistence.journal.plugin = \"akka.persistence.journal.inmem\" \n";

  private static final String snapshotConfig =
      "akka.persistence.snapshot-store.plugin = \"akka.persistence.snapshot-store.local\" \n"
      + "akka.persistence.snapshot-store.local.dir = \"target/snapshot-"
      + UUID.randomUUID().toString()
      + "\" \n";

  private static final String config = inmemConfig + snapshotConfig;

  @ClassRule
  public static final TestKitJunitResource testKit = new TestKitJunitResource(config);

  @Test
  public void testHello() {

      String id = "pizza";
      ActorRef<FridgeCommand> ref =
        testKit.spawn(
          FridgeAggregate.create(
            // Unit testing the Aggregate requires an EntityContext but starting
            // a complete Akka Cluster or sharding the actors is not required.
            // The actorRef to the shard can be null as it won't be used.
            new EntityContext(FridgeAggregate.ENTITY_TYPE_KEY, id,  null)
          )
        );

      TestProbe<FridgeCommand.Amount> probe =
        testKit.createTestProbe(FridgeCommand.Amount.class);
      ref.tell(new FridgeCommand.GetItemAmount(id,probe.getRef()));
      probe.expectMessage(new FridgeCommand.Amount("Item pizza has an amount of: Item not found"));
  }

  @Test
  public void testUpdateGreeting() {
      String id = "pizza";
      ActorRef<FridgeCommand> ref =
        testKit.spawn(
          FridgeAggregate.create(
            // Unit testing the Aggregate requires an EntityContext but starting
            // a complete Akka Cluster or sharding the actors is not required.
            // The actorRef to the shard can be null as it won't be used.
           new EntityContext(FridgeAggregate.ENTITY_TYPE_KEY, id,  null)
          )
        );

      TestProbe<FridgeCommand.Confirmation> probe1 =
        testKit.createTestProbe(FridgeCommand.Confirmation.class);
      ref.tell(new FridgeCommand.SetItemAmount("300 slices", probe1.getRef()));
      probe1.expectMessage(new FridgeCommand.Accepted());

      TestProbe<FridgeCommand.Amount> probe2 =
        testKit.createTestProbe(FridgeCommand.Amount.class);
      ref.tell(new FridgeCommand.GetItemAmount(id,probe2.getRef()));
      probe2.expectMessage(new FridgeCommand.Amount("Item pizza has an amount of: 300 slices"));
    }
}
