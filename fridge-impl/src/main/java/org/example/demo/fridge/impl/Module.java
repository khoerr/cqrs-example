package org.example.demo.fridge.impl;

import com.google.inject.AbstractModule;
import com.lightbend.lagom.javadsl.server.ServiceGuiceSupport;
import org.example.demo.fridge.api.FridgeService;

/**
 * The module that binds the FridgeService so that it can be served.
 */
public class Module extends AbstractModule implements ServiceGuiceSupport {
  @Override
  protected void configure() {
    bindService(FridgeService.class, FridgeServiceImpl.class);
  }
}
