package org.example.demo.fridge.impl;

import akka.cluster.sharding.typed.javadsl.EntityContext;
import akka.cluster.sharding.typed.javadsl.EntityTypeKey;
import akka.persistence.typed.PersistenceId;
import akka.persistence.typed.javadsl.*;
import com.lightbend.lagom.javadsl.persistence.AkkaTaggerAdapter;
import org.example.demo.fridge.impl.FridgeCommand.GetItemAmount;
import org.example.demo.fridge.impl.FridgeCommand.SetItemAmount;
import org.example.demo.fridge.impl.FridgeEvent.ItemAmountChanged;

import java.util.Set;

/**
* This is an event sourced aggregate. It has a state, {@link FridgeState}, which
* stores the amounts of each item for the fridge.
* <p>
* Event sourced aggregate are interacted with by sending them commands. This
* aggregate supports two commands, a {@link SetItemAmount} command, which is
* used to change the greeting, and a {@link GetItemAmount} command, which is a read
* only command which returns a greeting to the name specified by the command.
* <p>
* Commands may emit events, and it's the events that get persisted.
* Each event will have an event handler registered for it, and an
* event handler simply applies an event to the current state. This will be done
* when the event is first created, and it will also be done when the entity is
* loaded from the database - each event will be replayed to recreate the state
* of the aggregate.
* <p>
* This aggregate defines one event, the {@link ItemAmountChanged} event,
* which is emitted when a {@link SetItemAmount} command is received.
*/
public class FridgeAggregate extends EventSourcedBehaviorWithEnforcedReplies<FridgeCommand, FridgeEvent, FridgeState> {

  public static EntityTypeKey<FridgeCommand> ENTITY_TYPE_KEY =
    EntityTypeKey
      .create(FridgeCommand.class, "HelloAggregate");


  final private EntityContext<FridgeCommand> entityContext;
  final private String entityId;

  FridgeAggregate(EntityContext<FridgeCommand> entityContext) {
    super(
      PersistenceId.of(
          entityContext.getEntityTypeKey().name(),
          entityContext.getEntityId()
        )
      );
    this.entityContext = entityContext;
    this.entityId = entityContext.getEntityId();
  }

  public static FridgeAggregate create(EntityContext<FridgeCommand> entityContext) {
    return new FridgeAggregate(entityContext);
  }

  @Override
  public FridgeState emptyState() {
    return FridgeState.INITIAL;
  }


  @Override
  public CommandHandlerWithReply<FridgeCommand, FridgeEvent, FridgeState> commandHandler() {

    CommandHandlerWithReplyBuilder<FridgeCommand, FridgeEvent, FridgeState> builder = newCommandHandlerWithReplyBuilder();

    /*
     * Command handler for the SetItemAmount command.
     */
    builder.forAnyState()
      .onCommand(SetItemAmount.class, (state, cmd) ->
        Effect()
          .persist(new ItemAmountChanged(entityId, cmd.amount))
          .thenReply(cmd.replyTo, __ -> new FridgeCommand.Accepted())
      );

    /*
     * Command handler for the GetItemAmount command.
     */
    builder.forAnyState()
      .onCommand(GetItemAmount.class, (state, cmd) ->
        Effect().none()
          .thenReply(cmd.replyTo, __ -> new FridgeCommand.Amount("Item " + cmd.name + " has an amount of: " + state.amount))
      );

    return builder.build();

  }


  @Override
  public EventHandler<FridgeState, FridgeEvent> eventHandler() {
    EventHandlerBuilder<FridgeState, FridgeEvent> builder = newEventHandlerBuilder();

    /*
     * Event handler for the ItemAmountChanged event.
     */
    builder.forAnyState()
      .onEvent(ItemAmountChanged.class, (state, evt) ->
        state.withAmount(evt.amount)
      );
    return builder.build();
  }


  @Override
  public Set<String> tagsFor(FridgeEvent fridgeEvent) {
    return AkkaTaggerAdapter.fromLagom(entityContext, FridgeEvent.TAG).apply(fridgeEvent);
  }

}
