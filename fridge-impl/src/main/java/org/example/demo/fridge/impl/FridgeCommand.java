package org.example.demo.fridge.impl;

import akka.actor.typed.ActorRef;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.serialization.CompressedJsonable;
import com.lightbend.lagom.serialization.Jsonable;
import lombok.Value;

/**
  * This interface defines all the commands that the Hello aggregate supports.
  * <p>
  * By convention, the commands and replies should be inner classes of the
  * interface, which makes it simple to get a complete picture of what commands
  * an aggregate supports.
  */
public interface FridgeCommand extends Jsonable {

  /**
  * A command to switch the greeting message.
  * <p>
  * It has a reply type of {@link Confirmation}, which is sent back to the caller
  * when all the events emitted by this command are successfully persisted.
  */
  @SuppressWarnings("serial")
  @Value
  @JsonDeserialize
  final class SetItemAmount implements FridgeCommand, CompressedJsonable {
    public final String amount;
    public final ActorRef<Confirmation> replyTo;
    
    @JsonCreator
    SetItemAmount(String amount, ActorRef<Confirmation> replyTo) {
      this.amount = Preconditions.checkNotNull(amount, "amount");
      this.replyTo = replyTo;
    }
  }

  /**
  * A command to get the amount using the current greeting message.
  * <p>
  * The reply type is {@link Amount} and will contain the message to say to that
  * person.
  */
  @SuppressWarnings("serial")
  @Value
  @JsonDeserialize
  final class GetItemAmount implements FridgeCommand {
    public final String name;
    public final ActorRef<Amount> replyTo;
    
    @JsonCreator
    GetItemAmount(String name, ActorRef<Amount> replyTo) {
      this.name = Preconditions.checkNotNull(name, "name");
      this.replyTo = replyTo;
    }
  }

  // The commands above will use different reply types (see below all the reply types).
  
  /**
  * Super interface for Accepted/Rejected replies used by UseGreetingMessage
  */
  interface Confirmation {
  }

  @Value
  @JsonDeserialize
  final class Accepted implements Confirmation {
  }

  @Value
  @JsonDeserialize
  final class Rejected implements Confirmation {
    public final String reason;
    
    public Rejected(String reason) {
      this.reason = reason;
    }
  }

  /**
  * Reply type for a Fridge command.
  */
  @Value
  @JsonDeserialize
  final class Amount {
    public final String amount;
    
    public Amount(String amount) {
      this.amount = amount;
    }
  }

}
