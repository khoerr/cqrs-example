package org.example.demo.fridge.impl;

import lombok.Value;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.serialization.CompressedJsonable;

import java.time.LocalDateTime;

/**
 * The state for the {@link Fridge} aggregate.
 */
@SuppressWarnings("serial")
@Value
@JsonDeserialize
public final class FridgeState implements CompressedJsonable {
  public static final FridgeState INITIAL = new FridgeState("Item not found", LocalDateTime.now().toString());
  public final String amount;
  public final String timestamp;

  @JsonCreator
  public FridgeState(String amount, String timestamp) {
    this.amount = Preconditions.checkNotNull(amount, "amount");
    this.timestamp = Preconditions.checkNotNull(timestamp, "timestamp");
  }

  public FridgeState withAmount(String amount) {
    return new FridgeState(amount, LocalDateTime.now().toString());
  }
}
