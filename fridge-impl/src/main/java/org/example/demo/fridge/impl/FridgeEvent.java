package org.example.demo.fridge.impl;

import com.lightbend.lagom.javadsl.persistence.AggregateEvent;
import com.lightbend.lagom.javadsl.persistence.AggregateEventShards;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTag;
import com.lightbend.lagom.javadsl.persistence.AggregateEventTagger;
import lombok.Value;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.google.common.base.Preconditions;
import com.lightbend.lagom.serialization.Jsonable;

/**
 * This interface defines all the events that the Fridge aggregate supports.
 * <p>
 * By convention, the events should be inner classes of the interface, which
 * makes it simple to get a complete picture of what events an entity has.
 */
public interface FridgeEvent extends Jsonable, AggregateEvent<FridgeEvent> {

  /**
   * Tags are used for getting and publishing streams of events. Each event
   * will have this tag, and in this case, we are partitioning the tags into
   * 4 shards, which means we can have 4 concurrent processors/publishers of
   * events.
   */
  AggregateEventShards<FridgeEvent> TAG = AggregateEventTag.sharded(FridgeEvent.class, 4);

  /**
   * An event that represents a change in amount.
   */
  @SuppressWarnings("serial")
  @Value
  @JsonDeserialize
  public final class ItemAmountChanged implements FridgeEvent {

    public final String name;
    public final String amount;

    @JsonCreator
    public ItemAmountChanged(String name, String amount) {
      this.name = Preconditions.checkNotNull(name, "name");
      this.amount = Preconditions.checkNotNull(amount, "amount");
    }
  }

  @Override
  default AggregateEventTagger<FridgeEvent> aggregateTag() {
    return TAG;
  }

}
