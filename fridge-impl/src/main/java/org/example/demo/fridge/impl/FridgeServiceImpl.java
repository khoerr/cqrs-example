package org.example.demo.fridge.impl;

import akka.Done;
import akka.NotUsed;
import akka.cluster.sharding.typed.javadsl.ClusterSharding;
import akka.cluster.sharding.typed.javadsl.Entity;
import akka.cluster.sharding.typed.javadsl.EntityRef;
import akka.japi.Pair;
import com.lightbend.lagom.javadsl.api.ServiceCall;
import com.lightbend.lagom.javadsl.api.broker.Topic;
import com.lightbend.lagom.javadsl.api.transport.BadRequest;
import com.lightbend.lagom.javadsl.broker.TopicProducer;
import com.lightbend.lagom.javadsl.persistence.PersistentEntityRegistry;

import org.example.demo.fridge.api.FridgeService;
import org.example.demo.fridge.api.ItemAmount;

import javax.inject.Inject;
import java.time.Duration;

/**
 * Implementation of the FridgeService.
 */
public class FridgeServiceImpl implements FridgeService {

  private final PersistentEntityRegistry persistentEntityRegistry;

  private final Duration askTimeout = Duration.ofSeconds(5);
  private ClusterSharding clusterSharding;

  @Inject
  public FridgeServiceImpl(PersistentEntityRegistry persistentEntityRegistry, ClusterSharding clusterSharding){
    this.clusterSharding=clusterSharding;
    // The persistent entity registry is only required to build an event stream for the TopicProducer
    this.persistentEntityRegistry=persistentEntityRegistry;

    // register the Aggregate as a sharded entity
    this.clusterSharding.init(
    Entity.of(
    FridgeAggregate.ENTITY_TYPE_KEY,
    FridgeAggregate::create
    )
    );
  }

  @Override
  public ServiceCall<NotUsed, String> contents(String item) {
    return request -> {

    EntityRef<FridgeCommand> ref = clusterSharding.entityRefFor(FridgeAggregate.ENTITY_TYPE_KEY, item);

    return ref.
      <FridgeCommand.Amount>ask(replyTo -> new FridgeCommand.GetItemAmount(item, replyTo), askTimeout)
      .thenApply(am -> am.amount);
    };
  }

  @Override
  public ServiceCall<ItemAmount, Done> store(String item) {
    return request -> {

    EntityRef<FridgeCommand> ref = clusterSharding.entityRefFor(FridgeAggregate.ENTITY_TYPE_KEY, item);

    return ref.
      <FridgeCommand.Confirmation>ask(replyTo -> new FridgeCommand.SetItemAmount(request.amount, replyTo), askTimeout)
          .thenApply(confirmation -> {
              if (confirmation instanceof FridgeCommand.Accepted) {
                return Done.getInstance();
              } else {
                throw new BadRequest(((FridgeCommand.Rejected) confirmation).reason);
              }
          });
    };

  }

  @Override
  public Topic<org.example.demo.fridge.api.FridgeEvent> fridgeEvents() {
    // We want to publish all the shards of the fridge event
    return TopicProducer.taggedStreamWithOffset(FridgeEvent.TAG.allTags(), (tag, offset) ->

      // Load the event stream for the passed in shard tag
      persistentEntityRegistry.eventStream(tag, offset).map(eventAndOffset -> {

      // Now we want to convert from the persisted event to the published event.
      // Although these two events are currently identical, in future they may
      // change and need to evolve separately, by separating them now we save
      // a lot of potential trouble in future.
      org.example.demo.fridge.api.FridgeEvent eventToPublish;

      if (eventAndOffset.first() instanceof FridgeEvent.ItemAmountChanged) {
        FridgeEvent.ItemAmountChanged amountChanged = (FridgeEvent.ItemAmountChanged) eventAndOffset.first();
        eventToPublish = new org.example.demo.fridge.api.FridgeEvent.ItemAmountChanged(
          amountChanged.getName(), amountChanged.getAmount()
        );
      } else {
        throw new IllegalArgumentException("Unknown event: " + eventAndOffset.first());
      }

        // We return a pair of the translated event, and its offset, so that
        // Lagom can track which offsets have been published.
        return Pair.create(eventToPublish, eventAndOffset.second());
      })
    );
  }
}
